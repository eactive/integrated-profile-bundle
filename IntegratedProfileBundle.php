<?php

/*
 * This file is part of the Integrated package.
 *
 * (c) e-Active B.V. <integrated@e-active.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Integrated\Bundle\ProfileBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * @author Danny Hillebrand <danny@e-active.nl>
 */

class IntegratedProfileBundle extends Bundle
{
}
