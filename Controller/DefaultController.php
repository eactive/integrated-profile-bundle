<?php

namespace Integrated\Bundle\ProfileBundle\Controller;

use Integrated\Bundle\ProfileBundle\Document\Profile;
use Integrated\Bundle\ProfileBundle\Document\ProfileData\Page;
use Integrated\Bundle\ProfileBundle\Document\ProfileSession;
use Integrated\Bundle\ProfileBundle\Document\ProfileData\ProfileData;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('IntegratedProfileBundle:Default:index.html.twig', array('name' => $name));
    }

    public function ajaxAction(Request $request)
    {
        $dm = $this->get('doctrine_mongodb')->getManager();

        if ($request->isXMLHttpRequest() || 1 == 1) {

            $trackId = $request->cookies->get('trackId', null);
            $trackSId = $request->cookies->get('trackSId', null);

            if ($trackId) {
                $track = $dm->getRepository('IntegratedProfileBundle:Profile')->find($trackId);
            }

            if ($trackSId) {
                $trackS = $dm->getRepository('IntegratedProfileBundle:ProfileSession')->find($trackSId);
            }

            if (!$track) {
                $track = new Profile();
                $dm->persist($track);
            }

            if (!$trackS) {
                $trackS = new ProfileSession($track);
                $dm->persist($trackS);
            }

            $page = new Page();
            $page->setPageTitle($request->get('t', null));
            $page->setPageUrl($request->get('u', null));
            $trackS->addPage($page);

            $trackS->setIp($request->server->get('REMOTE_ADDR', null));

            $dm->flush();

            $response = new Response('');
            $response->headers->setCookie(new Cookie('trackId', $track->getId(), "+1 year"));
            $response->headers->setCookie(new Cookie('trackSId', $trackS->getId()));

            if ($exists == 'false') {
                $save = $this->get('integratedprofile.save_profile');
                $test = $save->saveAction();
                return $response;
            }else if ($exists == 'true'){
                return $response;
            } else {
                return $response;
            }

        }

        return new Response('This is not ajax', 400);
    }
}
