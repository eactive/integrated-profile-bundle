function ajaxTrack(id,exists) {
    $.ajax({
        type: "GET",
        url: "/app_dev.php/profile/ajax-update",
        data: {
            cookieId: id,
            cookieExists: exists
        },
        dataType: "json",
        success: function(response) {
            console.log(response);
        }
    });
};
ajaxTrack();


function checkCookie() {
    var id = getCookie("id");
    var exists;

    if (id != "") {
        //cookie exists
        exists = true;
        ajaxCall(id,exists);
    } else {
        //create cookie
        id = CreateID();
            if (id != "" && id != null) {
                exists = false;
                ajaxCall(id,exists);
                setCookie("id", id, 365);
        }
    }
}
