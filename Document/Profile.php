<?php

namespace Integrated\Bundle\ProfileBundle\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Doctrine\Common\Collections\Collection;
use Integrated\Bundle\ProfileBundle\Document\ProfileData\ProfileData;

/**
 * @ODM\Document
 */
class Profile
{
    /**
     * @var string
     * @ODM\Id(strategy="UUID")
     */
    protected $id;

    /**
     * @var \DateTime
     * @ODM\Date
     */
    protected $createdAt;

    /**
     * @var string
     * @ODM\string
     */
    protected $personID;

    /**
    * @var ProfileData[] | Collection
    * @ODM\EmbedMany(targetDocument="Integrated\Bundle\ProfileBundle\Document\ProfileData\ProfileData")
    */
    protected $profileDataComplete;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->profileDataComplete = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return string
     */
    public function getPersonID()
    {
        return $this->personID;
    }

    /**
     * @param string $personID
     * @return $this
     */
    public function setPersonID($personID)
    {
        $this->personID = $personID;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getProfileDataComplete()
    {
        return $this->profileDataComplete;
    }

    /**
     * @param Collection $profileDataComplete
     * @return $this
     */
    public function setProfileDataComplete(Collection $profileDataComplete)
    {
        $this->profileDataComplete = $profileDataComplete;
        return $this;
    }
}
