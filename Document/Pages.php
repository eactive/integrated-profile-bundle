<?php

namespace Integrated\Bundle\ProfileBundle\Document;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document
 */
class Pages
{
    /**
     * @var string
     * @ODM\String
     */
    protected $pageUrl;

    /**
     * @var string
     * @ODM\Id
     */
    protected $pageID;

    /**
     * @var string
     * @ODM\String
     */
    protected $pageTitle;

    /**
     * @var int
     * @ODM\Int
     */
    protected $totalCount;

    /**
     * @return string
     */
    public function getPageUrl()
    {
        return $this->pageUrl;
    }

    /**
     * @param string $pageUrl
     * @return $this
     */
    public function setPageUrl($pageUrl)
    {
        $this->pageUrl = $pageUrl;
        return $this;
    }

    /**
     * @return string
     */
    public function getPageID()
    {
        return $this->pageID;
    }

    /**
     * @param string $pageID
     * @return $this
     */
    public function setPageID($pageID)
    {
        $this->pageID = $pageID;
        return $this;
    }

    /**
     * @return string
     */
    public function getPageTitle()
    {
        return $this->pageTitle;
    }

    /**
     * @param string $pageTitle
     * @return $this
     */
    public function setPageTitle($pageTitle)
    {
        $this->pageTitle = $pageTitle;
        return $this;
    }

    /**
     * @return int
     */
    public function getTotalCount()
    {
        return $this->totalCount;
    }

    /**
     * @param int $totalCount
     * @return $this
     */
    public function setTotalCount($totalCount)
    {
        $this->totalCount = $totalCount;
        return $this;
    }
}
