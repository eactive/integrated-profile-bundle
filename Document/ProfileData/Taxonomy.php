<?php

namespace Integrated\Bundle\ProfileBundle\Document\ProfileData;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document
 */
class Taxonomy
{
    /**
     * @var string
     * @ODM\Id
     */
    protected $taxonomyId;

    /**
     * @var array
     * @ODM\Collection
     */
    protected $pageSubjects = [];

    /**
     * @return array
     */
    public function getPageSubjects()
    {
        return $this->pageSubjects;
    }

    /**
     * @param array $pageSubjects
     * @return $this
     */
    public function setPageSubjects(array $pageSubjects = [])
    {
        $this->pageSubjects = $pageSubjects;
        return $this;
    }

}

