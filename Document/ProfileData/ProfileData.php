<?php

namespace Integrated\Bundle\ProfileBundle\Document\ProfileData;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document
 */
class ProfileData
{
    /**
     * @var string
     * @ODM\Id
     */
    protected $pageId;

    /**
     * @var int
     * @ODM\Int
     */
    protected $totalCount;

    /**
     * @var string
     * @ODM\String
     */
    protected $devices;

    /**
     * @return string
     */
    public function getPageId()
    {
        return $this->pageId;
    }

    /**
     * @param string $pageId
     * @return $this
     */
    public function setPageId($pageId)
    {
        $this->pageId = $pageId;
        return $this;
    }

    /**
     * @return int
     */
    public function getTotalCount()
    {
        return $this->totalCount;
    }

    /**
     * @param int $totalCount
     * @return $this
     */
    public function setTotalCount($totalCount)
    {
        $this->totalCount = $totalCount;
        return $this;
    }

    /**
     * @return string
     */
    public function getDevices()
    {
        return $this->devices;
    }

    /**
     * @param string $devices
     * @return $this
     */
    public function setDevices($devices)
    {
        $this->devices = $devices;
        return $this;
    }
}
