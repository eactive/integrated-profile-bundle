<?php

namespace Integrated\Bundle\ProfileBundle\Document\ProfileData;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document
 */
class Location
{
    /**
     * @var string
     * @ODM\Id
     */
    protected $locationId;

    /**
     * @var string
     * @ODM\String
     */
    protected $country;

    /**
     * @var string
     * @ODM\String
     */
    protected $province;

    /**
     * @var string
     * @ODM\String
     */
    protected $city;

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     * @return $this
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * @param string $province
     * @return $this
     */
    public function setProvince($province)
    {
        $this->province = $province;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;
    }
}
