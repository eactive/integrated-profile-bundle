<?php

namespace Integrated\Bundle\ProfileBundle\Document\ProfileData;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document
 */
class Page
{
    /**
     * @var string
     * @ODM\Id
     */
    protected $pageIdId;

    /**
     * @var string
     * @ODM\String
     */
    protected $pageUrl;

    /**
     * @var string
     * @ODM\String
     */
    protected $pageId;

    /**
     * @var string
     * @ODM\String
     */
    protected $pageTitle;

    /**
     * @return string
     */
    public function getPageUrl()
    {
        return $this->pageUrl;
    }

    /**
     * @param string $pageUrl
     * @return $this
     */
    public function setPageUrl($pageUrl)
    {
        $this->pageUrl = $pageUrl;
    }

    /**
     * @return string
     */
    public function getPageId()
    {
        return $this->pageId;
    }

    /**
     * @param string $pageId
     * @return $this
     */
    public function setPageId($pageId)
    {
        $this->pageId = $pageId;
    }

    /**
     * @return string
     */
    public function getPageTitle()
    {
        return $this->pageTitle;
    }

    /**
     * @param string $pageTitle
     * @return $this
     */
    public function setPageTitle($pageTitle)
    {
        $this->pageTitle = $pageTitle;
    }
}
