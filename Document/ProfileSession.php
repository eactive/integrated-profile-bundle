<?php

namespace Integrated\Bundle\ProfileBundle\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Doctrine\Common\Collections\Collection;
use Integrated\Bundle\ProfileBundle\Document\ProfileData\Page;
use Integrated\Bundle\ProfileBundle\Document\ProfileData\Location;
use Integrated\Bundle\ProfileBundle\Document\ProfileData\Taxonomy;

/**
 * @ODM\Document
 */
class ProfileSession
{
    /**
     * @var string
     * @ODM\Id(strategy="UUID")
     */
    protected $id;

    /**
     * @var Profile
     * @ODM\ReferenceOne(targetDocument="Integrated\Bundle\ProfileBundle\Document\Profile")
     */
    protected $profile;

    /**
     * @var string
     * @ODM\String
     */
    protected $ip;

    /**
     * @var int
     * @ODM\Int
     */
    protected $duration;

    /**
     * @var string
     * @ODM\String
     */
    protected $device;

    /**
     * @var Page[] | Collection
     * @ODM\EmbedMany(targetDocument="Integrated\Bundle\ProfileBundle\Document\ProfileData\Page")
     */
    protected $pages;

    /**
     * @var Location[] | Collection
     * @ODM\EmbedMany(targetDocument="Integrated\Bundle\ProfileBundle\Document\ProfileData\Location")
     */
    protected $locations;

    /**
     * @var Taxonomy[] | Collection
     * @ODM\EmbedMany(targetDocument="Integrated\Bundle\ProfileBundle\Document\ProfileData\Taxonomy")
     */
    protected $taxonomy;

    /**
     * @var \DateTime
     * @ODM\Date
     */
    protected $date;

    /**
     * @var Collection
     * @ODM\Collection
     */
    protected $custom;

    /**
     * Constructor
     */
    public function __construct(Profile $profile)
    {
        $this->profile = $profile;
        $this->pages = [];
        $this->locations = [];
        $this->taxonomy = [];
        $this->custom = [];
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $profile
     * @return $this
     */
    public function setProfile($profile)
    {
        $this->profile = $profile;
        return $this;
    }

    /**
     * @return string
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     * @return $this
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
        return $this;
    }

    /**
     * @return int
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param int $duration
     * @return $this
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
        return $this;
    }

    /**
     * @return string
     */
    public function getDevice()
    {
        return $this->device;
    }

    /**
     * @param string $device
     * @return $this
     */
    public function setDevice($device)
    {
        $this->device = $device;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getPages()
    {
        return $this->pages;
    }

    /**
     * @param Collection $pages
     * @return $this
     */
    public function setPages(Collection $pages)
    {
        $this->pages = $pages;
        return $this;
    }

    /**
     * @param Page $page
     * @return $this
     */
    public function addPage(Page $page)
    {
        $this->pages[] = $page;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getLocations()
    {
        return $this->locations;
    }

    /**
     * @param Collection $locations
     * @return $this
     */
    public function setLocations(Collection $locations)
    {
        $this->locations = $locations;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getTaxonomy()
    {
        return $this->taxonomy;
    }

    /**
     * @param Collection $taxonomy
     * @return $this
     */
    public function setTaxonomy(Collection $taxonomy)
    {
        $this->taxonomy = $taxonomy;
        return $this;
    }

    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getCustom()
    {
        return $this->custom;
    }

    /**
     * @param Collection $custom
     * @return $this
     */
    public function setCustom(Collection $custom)
    {
        $this->custom = $custom;
        return $this;
    }
}
