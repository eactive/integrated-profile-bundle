<?php

namespace Integrated\Bundle\ProfileBundle\Service;

use Integrated\Bundle\ProfileBundle\Document\Profile;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
// use Doctrine\ODM\MongoDB as Manager;
use Symfony\Bridge\Doctrine\ManagerRegistry as Manager;

class SaveProfile
{
    private $container;
    private $doctrine;

    public function __construct(Container $container, Manager $doctrine)
    {
        $this->container = $container;
        $this->doctrine = $doctrine;
    }

    public function saveAction()
    {
        $profile = new Profile();

        $date = new \DateTime();
        $date->format('Y-m-d H:i:sP');
        $profile->setCreatedAt($date);

        if ($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $user = $this->container->get('security.token_storage')->getToken()->getUser()->getId();
            $profile->setPersonID($user);
        }

        $this->doctrine->getManager()->persist($profile);
        $this->doctrine->getManager()->flush();


        return 'Saved new profile.';
    }
}
