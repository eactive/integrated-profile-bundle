<?php

namespace Integrated\Bundle\ProfileBundle\Twig\Extension;




class AjaxExtension extends \Twig_Extension
{
    public function getName()
    {
        return 'profile_extension';
    }

    public function getData($article)
    {
        $id = $article->getId();
        $title = $article->getTitle();
        $slug = $article->getSlug();
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('integrated_profile_ajax',
                array( $this, 'renderAjax' ),
                array( 'is_safe' => ['html'], 'needs_environment' => true)
        ));
    }

    public function renderAjax(\Twig_Environment $twig, $article)
    {
        $this->getData($article);
        return $twig->render('IntegratedProfileBundle:Default:index.html.twig');
    }

}