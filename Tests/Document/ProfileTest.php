<?php

namespace Integrated\Bundle\ProfileBundle\Tests;

use Doctrine\Common\Collections\ArrayCollection;
use Integrated\Bundle\ProfileBundle\Document\Profile;

class ProfileTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Profile
     */
    private $profile;

    public function setUp()
    {
        $this->profile = new Profile();
    }

    /**
     * Test get- and setId function
     */
    public function testGetAndSetIdFunction()
    {
        $id = 'id';
        $this->assertSame($id, $this->profile->setId($id)->getId());
    }

    /**
     * Test get- and setCreatedAt function
     */
    public function testGetAndSetCreatedAtFunction()
    {
        $createdAt = new \DateTime();
        $this->assertSame($createdAt, $this->profile->setCreatedAt($createdAt)->getCreatedAt());
    }

    /**
     * Test get- and setPersonId function
     */
    public function testGetAndSetPersonIdFunction()
    {
        $personID = 'PersonID';
        $this->assertSame($personID, $this->profile->setPersonID($personID)->getPersonID());
    }

    /**
     * Test get- and setProfileDataComplete function
     */
    public function testGetAndSetProfileDataCompleteFunction()
    {
        $profileData = new ArrayCollection(array('key' => 'value'));
        $this->assertSame($profileData, $this->profile->setProfileDataComplete($profileData)->getProfileDataComplete());
    }
}
