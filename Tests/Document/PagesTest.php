<?php

namespace Integrated\Bundle\ProfileBundle\Tests;

use Integrated\Bundle\ProfileBundle\Document\Pages;

class PagesTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Pages
     */
    private $page;

    public function setUp()
    {
        $this->page = new Pages();
    }

    /**
     * Test get- and setPageUrl function
     */
    public function testGetAndSetPageUrlFunction()
    {
        $pageUrl = 'PageUrl';
        $this->assertSame($pageUrl, $this->page->setPageUrl($pageUrl)->getPageUrl());
    }

    /**
     * Test get- and setPageId function
     */
    public function testGetAndSetPageIdFunction()
    {
        $pageId = 'PageId';
        $this->assertSame($pageId, $this->page->setPageId($pageId)->getPageId());
    }

    /**
     * Test get- and setPageTitle function
     */
    public function testGetAndSetPageTitleFunction()
    {
        $pageTitle = 'PageTitle';
        $this->assertSame($pageTitle, $this->page->setPageTitle($pageTitle)->getPageTitle());
    }

    /**
     * Test get- and setTotalCount function
     */
    public function testGetAndSetTotalCountFunction()
    {
        $totalCount = '15';
        $this->assertSame($totalCount, $this->page->setTotalCount($totalCount)->getTotalCount());
    }
}
