<?php

namespace Integrated\Bundle\ProfileBundle\Tests;

use Integrated\Bundle\ProfileBundle\Document\ProfileSession;
use Doctrine\Common\Collections\ArrayCollection;

class ProfileSessionTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var ProfileSession
     */
    private $profileSession;

    public function setUp()
    {
        $this->profileSession = new ProfileSession();
    }

    /**
     * Test get- and setId function
     */
    public function testGetAndSetIdFunction()
    {
        $id = 'id';
        $this->assertSame($id, $this->profileSession->setId($id)->getId());
    }

    /**
     * Test get- and setProfileId function
     */
    public function testGetAndSetProfileIdFunction()
    {
        $profileId = 'id';
        $this->assertSame($profileId, $this->profileSession->setProfileId($profileId)->getProfileId());
    }

    /**
     * Test get- and setDuration function
     */
    public function testGetAndSetDurationFunction()
    {
        $duration = '15';
        $this->assertSame($duration, $this->profileSession->setDuration($duration)->getDuration());
    }

    /**
     * Test get- and setDevice function
     */
    public function testGetAndSetDeviceFunction()
    {
        $device = 'android';
        $this->assertSame($device, $this->profileSession->setDevice($device)->getDevice());
    }

    /**
     * Test get- and setPages function
     */
    public function testGetAndSetPagesFunction()
    {
        $pages = new ArrayCollection(array('key' => 'value'));
        $this->assertSame($pages, $this->profileSession->setPages($pages)->getPages());
    }

    /**
     * Test get- and setLocations function
     */
    public function testGetAndSetLocationsFunction()
    {
        $location = new ArrayCollection(array('key' => 'value'));
        $this->assertSame($location, $this->profileSession->setLocations($location)->getLocations());
    }

    /**
     * Test get- and setTaxonomy function
     */
    public function testGetAndSetTaxonomyFunction()
    {
        $taxonomy = new ArrayCollection(array('key' => 'value'));
        $this->assertSame($taxonomy, $this->profileSession->setTaxonomy($taxonomy)->getTaxonomy());
    }

    /**
     * Test get- and setDate function
     */
    public function testGetAndSetDateFunction()
    {
        $date = new \DateTime();
        $this->assertSame($date, $this->profileSession->setDate($date)->getDate());
    }

    /**
     * Test get- and setCustom function
     */
    public function testGetAndSetCustomFunction()
    {
        $custom = new ArrayCollection(array('key' => 'value'));
        $this->assertSame($custom, $this->profileSession->setCustom($custom)->getCustom());
    }
}
